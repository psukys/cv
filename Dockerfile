FROM ubuntu:18.04

WORKDIR /data

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get install -yq texlive-full \
                   make \
                   latexmk
